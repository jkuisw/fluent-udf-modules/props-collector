/*******************************************************************************
 * @file propertyReaders.h
 * Module: property readers
 * Description:
 *   This module implements some functions for reading simulation properties.
 */
/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "udf.h"

/** The user of the UDM manager has to create an header file which declares an
 * enum with all needed zone ID`s of the case. The enum type must have the
 * name "ZoneId_t", example:
 * typedef enum ZoneId {
 *   eZoneIdZone1 = 7,
 *   eZoneIdZone2 = 15,
 *   eZoneIdZone3 = 135
 * } ZoneId_t;
 */
#include "zoneIds.h"

#ifndef PROPERTY_READERS_H
#define PROPERTY_READERS_H

/*******************************************************************************
 * Macros
 ******************************************************************************/

/*******************************************************************************
 * type definitions
 ******************************************************************************/
typedef struct {
  real value;
  real location[ND_ND];
  real areaNormal[ND_ND];
} ScalarProperty_t;

typedef struct {
  ScalarProperty_t* props;
  int maxProps;
  int numValidProps;
} ScalarPropField_t;

typedef struct {
  real value[ND_ND];
  real location[ND_ND];
  real areaNormal[ND_ND];
} VectorProperty_t;

typedef struct {
  VectorProperty_t* props;
  int maxProps;
  int numValidProps;
} VectorPropField_t;

typedef real (*GetFaceValue_t) (face_t face, Thread* pThread);

/*******************************************************************************
 * variables
 ******************************************************************************/

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/
/*******************************************************************************
 * Calculates the moment on a given zone id, projected to the given axis.
 *
 * @param pDomain The domain of the zone.
 * @param zoneId The id of the zone for which the calculation should be
 *   performed.
 * @param pOrigin Array of length ND_ND, defining the rotation origin vector.
 * @param pAxis Array of length ND_ND, defining the rotation axis vector.
 *
 * @return The moment on the zone projected to the axis.
 */
real getTotalWallMoment(Domain* pDomain, ZoneId_t zoneId, real* pOrigin,
  real* pAxis);

/*******************************************************************************
 * Calculates the area weighted average value of a face quantitiy. The value of
 * the face quantity will be retrieved by calling the given function pointer
 * which get's the current face and the face thread passed as parameters.
 *
 * @param pDomain The domain of the zone.
 * @param zoneId The id of the zone for which the calculation should be
 *   performed.
 * @param pfGetVarValue The function pointer to get the quantity value. It will
 *   be called with the current face and the current face thread.
 *
 * @return The area weighted average value of the quantity.
 */
real getAreaWAvg(Domain* pDomain, ZoneId_t zoneId,
  GetFaceValue_t pfGetVarValue);

/*******************************************************************************
 * Get the velocitiy magnitude value of the velocity vector of the face
 * projected into the face area normal direction.
 *
 * @param face The face from which to get the velocity magnitude value.
 * @param pThread The face thread in which the face lives.
 *
 * @return The velocity magnitude value.
 */
real getFaceProjVMagn(face_t face, Thread* pThread);

/*******************************************************************************
 * Returns the pressure value from the given face in the given face thread.
 *
 * @param face The face from which to get the pressure value.
 * @param pThread The face thread in which the face lives.
 *
 * @return The pressure value of the face.
 */
real getFacePressure(face_t face, Thread* pThread);

/*******************************************************************************
 * Returns an array of face y+ values of the given thread with their
 * corresponding location and area normal vectors.
 *
 * @param pThread The thread from which the y+ values should be retrieved.
 *
 * @return y+ values scalar field of the thread.
 */
ScalarPropField_t getYPlusValueFieldOfFaceThread(Thread* pThread);

/*******************************************************************************
 * Returns an array of face static pressure values of the given thread with
 * their corresponding location and area normal vectors.
 *
 * @param pThread The thread from which the static pressure values should be
 *   retrieved.
 *
 * @return Static pressure values scalar field of the thread.
 */
ScalarPropField_t getStaticPressureFieldOfFaceThread(Thread* pThread);

/*******************************************************************************
 * Returns an array of face velocity vectors of the given thread with their
 * corresponding location and area normal vectors.
 *
 * @param pThread The thread from which the velocities should be retrieved.
 *
 * @return Velocity vector field of the thread.
 */
VectorPropField_t getVelocityFieldOfFaceThread(Thread* pThread);

#endif /* PROPERTY_READERS_H */
