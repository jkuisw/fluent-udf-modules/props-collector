/*******************************************************************************
 * @file propertyReaders.h
 * Module: property readers
 * Description:
 *   This module implements some functions for reading simulation properties.
 */
/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "propertyReaders.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/

/*******************************************************************************
 * type definitions
 ******************************************************************************/

/*******************************************************************************
 * variables
 ******************************************************************************/

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/

/*******************************************************************************
 * Functions
 ******************************************************************************/
/*******************************************************************************
 * Calculates the moment on a given zone id, projected to the given axis.
 *
 * @param pDomain The domain of the zone.
 * @param zoneId The id of the zone for which the calculation should be
 *   performed.
 * @param pOrigin Array of length ND_ND, defining the rotation origin vector.
 * @param pAxis Array of length ND_ND, defining the rotation axis vector.
 *
 * @return The moment on the zone projected to the axis.
 */
real getTotalWallMoment(Domain* pDomain, ZoneId_t zoneId, real* pOrigin,
  real* pAxis) {
  /* Variables for all processes. */
  real totalMomentVector[ND_ND];
  real axisMagn = NV_MAG(pAxis);
  real axisNorm[ND_ND];

#if !RP_HOST
  /* Variables for compute node and serial processes. */
  real faceCentroid[ND_ND];
  real momentVector[ND_ND];
  real pressureForce[ND_ND];
  real viscousForce[ND_ND];
  real pressureMoment[ND_ND];
  real viscousMoment[ND_ND];
  real faceMoment[ND_ND];
  real faceAreaNormal[ND_ND];
  face_t face;
  Thread* pThread = NULL;
#endif /* !RP_HOST */

#if RP_NODE
  /* Compute node processes only variables. */
  real iwork[ND_ND];
#endif /* RP_NODE */

  /* Initialize variables for all processes. */
  NV_S(totalMomentVector, =, 0.0);
  NV_S(axisNorm, =, 0.0);
  if (axisMagn != 0) NV_VS(axisNorm, =, pAxis, /, axisMagn);

#if !RP_HOST
  /* Initialize compute node and serial processes only variables. */
  NV_S(faceCentroid, =, 0.0);
  NV_S(momentVector, =, 0.0);
  NV_S(pressureForce, =, 0.0);
  NV_S(viscousForce, =, 0.0);
  NV_S(pressureMoment, =, 0.0);
  NV_S(viscousMoment, =, 0.0);
  NV_S(faceMoment, =, 0.0);
  NV_S(faceAreaNormal, =, 0.0);
  pThread = Lookup_Thread(pDomain, zoneId);

  /* Calculate and sum up the moment on all boundary faces. */
  begin_f_loop(face, pThread) {
    if (PRINCIPAL_FACE_P(face, pThread)) {
      /* Get face position and area normal. */
      F_CENTROID(faceCentroid, face, pThread);
      F_AREA(faceAreaNormal, face, pThread);

      /* Calculate the vector from the moment origin to the face position. */
      NV_VV(momentVector, =, faceCentroid, -, pOrigin);

      /* Get the viscous and presure forces. */
      NV_VS(pressureForce, =, faceAreaNormal, *, F_P(face, pThread));
      NV_VS(viscousForce, =, F_STORAGE_R_NV(face, pThread, SV_WALL_SHEAR), *, -1);

      /* Calculate the face moment. */
      NV_CROSS(pressureMoment, momentVector, pressureForce);
      NV_CROSS(viscousMoment, momentVector, viscousForce);
      NV_VV(faceMoment, =, pressureMoment, +, viscousMoment);

      /* Add face moment to the total moment of the boundary zone. */
      NV_V(totalMomentVector, +=, faceMoment);
    }
  } end_f_loop(face, pThread)
#endif /* !RP_HOST */

#if RP_NODE
  /* Sum up all total moment vector values of all compute nodes. */
  PRF_GRSUM(totalMomentVector, ND_ND, iwork);
#endif /* RP_NODE */

/* Send total moment vector from compute nodes to host. */
node_to_host_real(totalMomentVector, ND_ND);

#if !RP_NODE
  /* Return the boundary zone moment in axis direction. */
  return NV_DOT(totalMomentVector, axisNorm);
#else
  /* Compute node processes always return zero. */
  return 0;
#endif
}

/*******************************************************************************
 * Calculates the area weighted average value of a face quantitiy. The value of
 * the face quantity will be retrieved by calling the given function pointer
 * which get's the current face and the face thread passed as parameters.
 *
 * @param pDomain The domain of the zone.
 * @param zoneId The id of the zone for which the calculation should be
 *   performed.
 * @param pfGetVarValue The function pointer to get the quantity value. It will
 *   be called with the current face and the current face thread.
 *
 * @return The area weighted average value of the quantity.
 */
real getAreaWAvg(Domain* pDomain, ZoneId_t zoneId,
  GetFaceValue_t pfGetVarValue) {
  /* Variables for all processes. */
  real areaValueProdSum = 0.0;
  real totalArea = 0.0;

#if !RP_HOST
  /* Variables for compute node and serial processes. */
  real value = 0.0;
  real faceArea = 0.0;
  real faceAreaNormal[ND_ND];
  face_t face;
  Thread* pThread;
#endif /* !RP_HOST */

#if !RP_HOST
  /* Initialize compute node and serial processes only variables. */
  NV_S(faceAreaNormal, =, 0.0);
  pThread = Lookup_Thread(pDomain, zoneId);

  /* Loop through all faces and sum up area and area value product. */
  begin_f_loop(face, pThread) {
    if (PRINCIPAL_FACE_P(face, pThread)) {
      /* Get face area. */
      F_AREA(faceAreaNormal, face, pThread);
      faceArea = NV_MAG(faceAreaNormal);

      /* Get value of face. */
      value = pfGetVarValue(face, pThread);

      /* Calculate area value product and add to sum. */
      areaValueProdSum += faceArea * value;
      /* Add face area to total area sum. */
      totalArea += faceArea;
    }
  } end_f_loop(face, pThread);
#endif /* !RP_HOST */

#if RP_NODE
  /* Sum up value product sums and total area values of all compute nodes. */
  areaValueProdSum = PRF_GRSUM1(areaValueProdSum);
  totalArea = PRF_GRSUM1(totalArea);
#endif /* RP_NODE */

/* Send area value product sum and total area from compute nodes to host. */
node_to_host_real_2(areaValueProdSum, totalArea);

#if !RP_NODE
  /* Calculate area weighted average of value and return it. */
  return areaValueProdSum / totalArea;
#else
  /* Compute node processes always return zero. */
  return 0;
#endif
}

/*******************************************************************************
 * Get the velocitiy magnitude value of the velocity vector of the face
 * projected into the face area normal direction.
 *
 * @param face The face from which to get the velocity magnitude value.
 * @param pThread The face thread in which the face lives.
 *
 * @return The velocity magnitude value.
 */
real getFaceProjVMagn(face_t face, Thread* pThread) {
  real faceAreaNormal[ND_ND];
  real normFaceAreaNormal[ND_ND];
  real faceVelocity[ND_ND];
  real faceAreaNormalMagn = 0.0;

  /* initialize variables */
  NV_S(faceAreaNormal, =, 0.0);
  NV_S(normFaceAreaNormal, =, 0.0);
  NV_S(faceVelocity, =, 0.0);

  /* Get face area normal and norm it. */
  F_AREA(faceAreaNormal, face, pThread);
  faceAreaNormalMagn = NV_MAG(faceAreaNormal);
  if (faceAreaNormalMagn != 0) {
    NV_VS(normFaceAreaNormal, =, faceAreaNormal, /, faceAreaNormalMagn);
  }

  /* Get face velocity vector. */
  faceVelocity[0] = F_U(face, pThread);
  faceVelocity[1] = F_V(face, pThread);
  faceVelocity[2] = F_W(face, pThread);

  /* Project velocity to area normal. */
  return ABS(NV_DOT(faceVelocity, normFaceAreaNormal));
}

/*******************************************************************************
 * Returns the pressure value from the given face in the given face thread.
 *
 * @param face The face from which to get the pressure value.
 * @param pThread The face thread in which the face lives.
 *
 * @return The pressure value of the face.
 */
real getFacePressure(face_t face, Thread* pThread) {
  return F_P(face, pThread);
}

/*******************************************************************************
 * Returns an array of face y+ values of the given thread with their
 * corresponding location and area normal vectors.
 *
 * @param pThread The thread from which the y+ values should be retrieved.
 *
 * @return y+ values scalar field of the thread.
 */
ScalarPropField_t getYPlusValueFieldOfFaceThread(Thread* pThread) {
  face_t face;
  ScalarPropField_t field;
  ScalarProperty_t prop;

  /* Initialize variables. */
  prop.value = 0.0;
  NV_S(prop.location, =, 0.0);
  NV_S(prop.areaNormal, =, 0.0);

  /* Allocate memory for the scalar field. */
  field.numValidProps = 0;
  field.maxProps = THREAD_N_ELEMENTS_INT(pThread);
  field.props = (ScalarProperty_t*)malloc(
    field.maxProps * sizeof(ScalarProperty_t));

  if (field.props == NULL) {
    Message("Failed to allocate %d memory for the y+ values field.\n",
      field.maxProps * sizeof(ScalarProperty_t));
    return field;
  }

  /* Loop through all faces and retrieve the y+ value. */
  begin_f_loop(face, pThread) {
    if (PRINCIPAL_FACE_P(face, pThread)) {
      /* Get face position and area normal. */
      F_CENTROID(prop.location, face, pThread);
      F_AREA(prop.areaNormal, face, pThread);

      /* Get the y+ value of the face. */
      prop.value = F_STORAGE_R(face, pThread, SV_WALL_YPLUS);

      field.props[field.numValidProps] = prop;
      field.numValidProps += 1;
    }
  } end_f_loop(face, pThread);

  return field;
}

/*******************************************************************************
 * Returns an array of face static pressure values of the given thread with
 * their corresponding location and area normal vectors.
 *
 * @param pThread The thread from which the static pressure values should be
 *   retrieved.
 *
 * @return Static pressure values scalar field of the thread.
 */
ScalarPropField_t getStaticPressureFieldOfFaceThread(Thread* pThread) {
  face_t face;
  ScalarPropField_t field;
  ScalarProperty_t prop;

  /* Initialize variables. */
  prop.value = 0.0;
  NV_S(prop.location, =, 0.0);
  NV_S(prop.areaNormal, =, 0.0);

  /* Allocate memory for the scalar field. */
  field.numValidProps = 0;
  field.maxProps = THREAD_N_ELEMENTS_INT(pThread);
  field.props = (ScalarProperty_t*)malloc(
    field.maxProps * sizeof(ScalarProperty_t));

  if (field.props == NULL) {
    Message("Failed to allocate %d memory for the static pressure field.\n",
      field.maxProps * sizeof(ScalarProperty_t));
    return field;
  }

  /* Loop through all faces and retrieve the static pressure values. */
  begin_f_loop(face, pThread) {
    if (PRINCIPAL_FACE_P(face, pThread)) {
      /* Get face position and area normal. */
      F_CENTROID(prop.location, face, pThread);
      F_AREA(prop.areaNormal, face, pThread);

      /* Get the static pressure value of the face. */
      prop.value = F_P(face, pThread);

      field.props[field.numValidProps] = prop;
      field.numValidProps += 1;
    }
  } end_f_loop(face, pThread);

  return field;
}

/*******************************************************************************
 * Returns an array of face velocity vectors of the given thread with their
 * corresponding location and area normal vectors.
 *
 * @param pThread The thread from which the velocities should be retrieved.
 *
 * @return Velocity vector field of the thread.
 */
VectorPropField_t getVelocityFieldOfFaceThread(Thread* pThread) {
  face_t face;
  VectorPropField_t field;
  VectorProperty_t prop;

  /* Initialize variables. */
  NV_S(prop.value, =, 0.0);
  NV_S(prop.location, =, 0.0);
  NV_S(prop.areaNormal, =, 0.0);

  /* Allocate memory for the vector field. */
  field.numValidProps = 0;
  field.maxProps = THREAD_N_ELEMENTS_INT(pThread);
  field.props = (VectorProperty_t*)malloc(
    field.maxProps * sizeof(VectorProperty_t));

  if (field.props == NULL) {
    Message("Failed to allocate %d memory for the velocity field.\n",
      field.maxProps * sizeof(VectorProperty_t));
    return field;
  }

  /* Loop through all faces and retrieve the velocity values. */
  begin_f_loop(face, pThread) {
    if (PRINCIPAL_FACE_P(face, pThread)) {
      /* Get face position and area normal. */
      F_CENTROID(prop.location, face, pThread);
      F_AREA(prop.areaNormal, face, pThread);

      /* Get face velocity vector. */
      prop.value[0] = F_U(face, pThread);
      prop.value[1] = F_V(face, pThread);
      prop.value[2] = F_W(face, pThread);

      field.props[field.numValidProps] = prop;
      field.numValidProps += 1;
    }
  } end_f_loop(face, pThread);

  return field;
}
