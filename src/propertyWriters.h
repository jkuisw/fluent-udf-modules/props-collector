/*******************************************************************************
 * @file propertyWriters.h
 * Module: property writers
 * Description:
 *   With this module simulation properties can be written to files.
 */
/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "udf.h"
#include "propertyReaders.h"

/** The user of the property writer module has to create an header file which
 * declares an enum with all needed zone ID`s of the case. The enum type must
 * have the name "ZoneId_t", example:
 * typedef enum ZoneId {
 *   eZoneIdZone1 = 7,
 *   eZoneIdZone2 = 15,
 *   eZoneIdZone3 = 135
 * } ZoneId_t;
 */
#include "zoneIds.h"

#ifndef PROPERTY_WRITERS_H
#define PROPERTY_WRITERS_H

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define WRITE_VECTOR_TO_FILE(pf, numFormat, v) \
  if (ND_ND == 2) { \
    fprintf(pf, numFormat " " numFormat, v[0], v[1]); \
  } else if (ND_ND == 3) { \
    fprintf(pf, numFormat " " numFormat " " numFormat, \
      v[0], v[1], v[3]); \
  }

/*******************************************************************************
 * type definitions
 ******************************************************************************/
typedef enum PropertyType {
  ePropTypeScalar,
  ePropTypeVector
} PropertyType_t;

typedef ScalarPropField_t (*GetScalarPropField_t) (Thread* pThread);
typedef VectorPropField_t (*GetVectorPropField_t) (Thread* pThread);

typedef struct {
  int domainId;
  ZoneId_t zoneId;
  PropertyType_t propType;
  const char* defaultFilePath;
  const char* filePathRpVarName;
  const char* propertyDescription;
  union {
    GetScalarPropField_t scalarField;
    GetVectorPropField_t vectorField;
  } propReader;
} PropertyToFileDef_t;

/*******************************************************************************
 * variables
 ******************************************************************************/

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/
/*******************************************************************************
 * Writes the properties, defined in the given array, to a file.
 *
 * @param propsToFileDefArray The array which contains the settings of which
 *   properties to read, where to write and how the property value can be read.
 *   For example a definition of this array could look as follows:
 *   const PropertyToFileDef_t propToFileDef[] = {
 *     {
 *       .domainId = CASE_DOMAIN_ID,
 *       .zoneId = eZoneIdLaufradWall,
 *       .propType = ePropTypeScalar,
 *       .defaultFilePath = "./yplus-values-laufrad-wall.txt",
 *       .filePathRpVarName = "yplus-values-file-path",
 *       .propertyDescription = "y+ values at laufrad wall",
 *       .propReader.scalarField = getYPlusValueFieldOfFaceThread
 *     },
 *     {
 *       .domainId = CASE_DOMAIN_ID,
 *       .zoneId = eZoneIdInlet,
 *       .propType = ePropTypeScalar,
 *       .defaultFilePath = "./p-static-inlet.txt",
 *       .filePathRpVarName = "p-st-inlet-file-path",
 *       .propertyDescription = "static pressure at inlet",
 *       .propReader.scalarField = getStaticPressureFieldOfFaceThread
 *     },
 *     {
 *       .domainId = CASE_DOMAIN_ID,
 *       .zoneId = eZoneIdInlet,
 *       .propType = ePropTypeVector,
 *       .defaultFilePath = "./v-inlet.txt",
 *       .filePathRpVarName = "v-inlet-file-path",
 *       .propertyDescription = "velocity at inlet",
 *       .propReader.vectorField = getVelocityFieldOfFaceThread
 *     }
 *   };
 * @param size The size of the array.
 */
void writePropertiesToFile(const PropertyToFileDef_t* propsToFileDef, int size);

/*******************************************************************************
 * Writes a scalar field to a file. The field will be retrieved by the given
 * function pointer. The property values, location vectors and area normal
 * vectors will be written to the file.
 *
 * @param pDomain The domain from which to read the property scalar field.
 * @param zoneId The id of the zone in the domain from which the property scalar
 *   field should be read.
 * @param pFile The file handle to write the data to.
 * @param propName The name of the scalar field property, will be used for
 *   the column name of the property values in the file.
 * @param pfGetVectorPropField The function pointer to retrieve the property
 *   scalar field.
 */
void writeScalarPropFieldToFile(Domain* pDomain, ZoneId_t zoneId, FILE* pFile,
  const char* propName, GetScalarPropField_t pfGetScalarPropField);

/*******************************************************************************
 * Writes a vector field to a file. The field will be retrieved by the given
 * function pointer. The property vectors, location vectors and area normal
 * vectors will be written to the file.
 *
 * @param pDomain The domain from which to read the property vector field.
 * @param zoneId The id of the zone in the domain from which the property vector
 *   field should be read.
 * @param pFile The file handle to write the data to.
 * @param propName The name of the vector field property, will be used for
 *   the column name of the property vectors in the file.
 * @param pfGetVectorPropField The function pointer to retrieve the property
 *   vector field.
 */
void writeVectorPropFieldToFile(Domain* pDomain, ZoneId_t zoneId, FILE* pFile,
  const char* propName, GetVectorPropField_t pfGetVectorPropField);

#endif /* PROPERTY_WRITERS_H */
