/*******************************************************************************
 * @file propertyWriters.c
 * Module: property writers
 * Description:
 *   With this module simulation properties can be written to files.
 */
/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "propertyWriters.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/

/*******************************************************************************
 * type definitions
 ******************************************************************************/

/*******************************************************************************
 * variables
 ******************************************************************************/

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/

/*******************************************************************************
 * Functions
 ******************************************************************************/
/*******************************************************************************
 * Writes the properties, defined in the given array, to a file.
 *
 * @param propsToFileDef An array which contains the settings of which
 *   properties to read, where to write and how the property value can be read.
 *   For example a definition of this array could look as follows:
 *   const PropertyToFileDef_t propsToFileDef[] = {
 *     {
 *       .domainId = CASE_DOMAIN_ID,
 *       .zoneId = eZoneIdLaufradWall,
 *       .propType = ePropTypeScalar,
 *       .defaultFilePath = "./yplus-values-laufrad-wall.txt",
 *       .filePathRpVarName = "yplus-values-file-path",
 *       .propertyDescription = "y+ values at laufrad wall",
 *       .propReader.scalarField = getYPlusValueFieldOfFaceThread
 *     },
 *     {
 *       .domainId = CASE_DOMAIN_ID,
 *       .zoneId = eZoneIdInlet,
 *       .propType = ePropTypeScalar,
 *       .defaultFilePath = "./p-static-inlet.txt",
 *       .filePathRpVarName = "p-st-inlet-file-path",
 *       .propertyDescription = "static pressure at inlet",
 *       .propReader.scalarField = getStaticPressureFieldOfFaceThread
 *     },
 *     {
 *       .domainId = CASE_DOMAIN_ID,
 *       .zoneId = eZoneIdInlet,
 *       .propType = ePropTypeVector,
 *       .defaultFilePath = "./v-inlet.txt",
 *       .filePathRpVarName = "v-inlet-file-path",
 *       .propertyDescription = "velocity at inlet",
 *       .propReader.vectorField = getVelocityFieldOfFaceThread
 *     }
 *   };
 *   If the variable "filePathRpVarName" expands to a valid RP variable, then
 *   the filepath where to write the property data will be read from the RP
 *   variable.
 * @param size The size of the array.
 */
void writePropertiesToFile(const PropertyToFileDef_t* propsToFileDef, int size) {
  Domain* pDomain = NULL;
  FILE* pFile = NULL;
  int i = 0;

#if !RP_NODE
  char* filePath = NULL;
  Message("\nBegin writing properties to files.\n");
#endif /* !RP_NODE */

  for (i = 0; i < size; i += 1) {
#if !RP_NODE
    /* Check if file path is given by RP variable, if not take default. */
    if (RP_Variable_Exists_P((char*)propsToFileDef[i].filePathRpVarName)) {
      filePath = RP_Get_String((char*)propsToFileDef[i].filePathRpVarName);
    } else {
      filePath = ((char*)propsToFileDef[i].defaultFilePath);
    }

    Message("Writing \"%s\" to the file \"%s\".\n",
      propsToFileDef[i].propertyDescription, filePath);
#endif /* !RP_NODE */

#if !RP_HOST
    pDomain = Get_Domain(propsToFileDef[i].domainId);
#endif /* !RP_HOST */

#if !RP_NODE
    pFile = fopen(filePath, "w");

    if (pFile == NULL) {
      Message("Failed to open file: %s\n", filePath);
    }
#endif /* !RP_NODE */

    /* Call the property field writer function. */
    switch (propsToFileDef[i].propType) {
      case ePropTypeScalar:
        if (propsToFileDef[i].propReader.scalarField != NULL) {
          writeScalarPropFieldToFile(
            pDomain,
            propsToFileDef[i].zoneId,
            pFile,
            propsToFileDef[i].propertyDescription,
            propsToFileDef[i].propReader.scalarField
          );
        } else {
          Message("Want to write a scalar field, but property reader for "
            "scalar field is NULL! Property description: %s\n",
            propsToFileDef[i].propertyDescription);
        }
        break;
      case ePropTypeVector:
        if (propsToFileDef[i].propReader.vectorField != NULL) {
          writeVectorPropFieldToFile(
            pDomain,
            propsToFileDef[i].zoneId,
            pFile,
            propsToFileDef[i].propertyDescription,
            propsToFileDef[i].propReader.vectorField
          );
        } else {
          Message("Want to write a scalar field, but property reader for "
            "scalar field is NULL! Property description: %s\n",
            propsToFileDef[i].propertyDescription);
        }
        break;
      default:
        Message("Unknown property type: %d\n", propsToFileDef[i].propType);
        break;
    }

#if !RP_NODE
    if (pFile != NULL) { fclose(pFile); }
#endif /* !RP_NODE */
  }

#if !RP_NODE
  Message("Finished.\n");
#endif /* !RP_NODE */
}

/*******************************************************************************
 * Writes a scalar field to a file. The field will be retrieved by the given
 * function pointer. The property values, location vectors and area normal
 * vectors will be written to the file.
 *
 * @param pDomain The domain from which to read the property scalar field.
 * @param zoneId The id of the zone in the domain from which the property scalar
 *   field should be read.
 * @param pFile The file handle to write the data to.
 * @param propName The name of the scalar field property, will be used for
 *   the column name of the property values in the file.
 * @param pfGetVectorPropField The function pointer to retrieve the property
 *   scalar field.
 */
void writeScalarPropFieldToFile(Domain* pDomain, ZoneId_t zoneId, FILE* pFile,
  const char* propName, GetScalarPropField_t pfGetScalarPropField) {
  ScalarPropField_t field;

#if !RP_HOST
  Thread* pThread = NULL;
#endif /* !RP_HOST */

#if PARALLEL
  int compNode = 0;
#endif /* PARALLEL */

#if !RP_NODE
  int i = 0;
#endif /* !RP_NODE */

  /* Initialize variables. */
  field.maxProps = 0;
  field.numValidProps = 0;
  field.props = NULL;

#if !RP_NODE
  /* Write file header. */
  if (pFile != NULL) {
    if (ND_ND == 2) {
      fprintf(pFile,
        "\"location\"                               "
      /* +0.000000000000e-00 +0.000000000000e-00  */
        "\"area normal\"                            "
      /* +0.000000000000e-00 +0.000000000000e-00  */
        "\"%s\"\n"
      /* +0.000000000000e-00 */
        "\"x-component\"       \"y-component\"        "
      /* +0.000000000000e-00 +0.000000000000e-00  */
        "\"x-component\"       \"y-component\"        "
      /* +0.000000000000e-00 +0.000000000000e-00  */
        "\"scalar value\"\n", propName);
      /* +0.000000000000e-00 */
    } else if (ND_ND == 3) {
      fprintf(pFile,
        "\"location\"                                                   "
      /* +0.000000000000e-00 +0.000000000000e-00 +0.000000000000e-00  */
        "\"area normal\"                                                "
      /* +0.000000000000e-00 +0.000000000000e-00 +0.000000000000e-00  */
        "\"%s\"\n"
      /* +0.000000000000e-00 +0.000000000000e-00 +0.000000000000e-00 */
        "\"x-component\"       \"y-component\"       \"z-component\"        "
      /* +0.000000000000e-00 +0.000000000000e-00 +0.000000000000e-00  */
        "\"x-component\"       \"y-component\"       \"z-component\"        "
      /* +0.000000000000e-00 +0.000000000000e-00 +0.000000000000e-00  */
        "\"scalar value\"\n", propName);
      /* +0.000000000000e-00 */
    }
  }
#endif /* !RP_NODE */

#if !RP_HOST
  /* Every compute node retrieves the scalar field of the property. */
  pThread = Lookup_Thread(pDomain, zoneId);
  field = pfGetScalarPropField(pThread);

# if RP_NODE
  /* compute nodes only */
  /* Every compute node, except compute node zero sends their field to the
   * compute node zero.
   */
  if (!I_AM_NODE_ZERO_P) {
    PRF_CSEND_INT(node_zero, &field.numValidProps, 1, myid);

    /* Skip if there are no properties to send. */
    if (field.numValidProps > 0) {
      PRF_CSEND_CHAR(node_zero, ((char*)field.props),
        field.numValidProps * sizeof(ScalarProperty_t), myid);
    }

    free(field.props);
  }

  /* Compute node zero sends it`s field to the host, than retrieves the fields
   * of the other compute nodes and sends them also to the host.
   */
  if (I_AM_NODE_ZERO_P) {
    /* Send node zero data to host. */
    PRF_CSEND_INT(node_host, &field.numValidProps, 1, myid);

    /* Skip if there are no properties to send. */
    if (field.numValidProps > 0) {
      PRF_CSEND_CHAR(node_host, ((char*)field.props),
        field.numValidProps * sizeof(ScalarProperty_t), myid);
    }

    free(field.props);
    field.props = NULL;

    /* Receive data from the compute nodes and send them to the host. */
    compute_node_loop_not_zero(compNode) {
      /* Get number of valid properties to receive from compute node. */
      PRF_CRECV_INT(compNode, &field.numValidProps, 1, compNode);
      field.maxProps = field.numValidProps;

      /* Skip if there are no properties to receive. */
      if (field.numValidProps > 0) {
        /* Allocate memory for the properties to receive. */
        field.props = (ScalarProperty_t*)malloc(
          field.numValidProps * sizeof(ScalarProperty_t));

        /* Check if successfully allocated memory. */
        if (field.props == NULL) {
          Message("Failed to allocate %d memory for a scalar field.\n",
            field.numValidProps * sizeof(ScalarProperty_t));
          return;
        }

        /* Retrieve data from the compute node. */
        PRF_CRECV_CHAR(compNode, ((char*)field.props),
          field.numValidProps * sizeof(ScalarProperty_t), compNode);
      }

      /* Send received compute node data from compute node zero data to the
      * host.
      */
      PRF_CSEND_INT(node_host, &field.numValidProps, 1, myid);

      /* Skip if there are no properties to receive. */
      if (field.props != NULL) {
        if (field.numValidProps > 0) {
          PRF_CSEND_CHAR(node_host, ((char*)field.props),
            field.numValidProps * sizeof(ScalarProperty_t), myid);
        }

        free(field.props);
        field.props = NULL;
      }
    }
  }
# endif /* RP_NODE */
#endif /* !RP_HOST */

#if RP_HOST
  compute_node_loop(compNode) {
    /* Get number of valid properties to receive from compute node. */
    PRF_CRECV_INT(node_zero, &field.numValidProps, 1, node_zero);
    field.maxProps = field.numValidProps;

    /* Skip if there are no properties to receive. */
    if (field.numValidProps > 0) {
      /* Allocate memory for the properties to receive. */
      field.props = (ScalarProperty_t*)malloc(
        field.numValidProps * sizeof(ScalarProperty_t));

      /* Check if successfully allocated memory. */
      if (field.props == NULL) {
        Message("Failed to allocate %d memory for a scalar field.\n",
          field.numValidProps * sizeof(ScalarProperty_t));
        return;
      }

      /* Retrieve data from the compute node. */
      PRF_CRECV_CHAR(node_zero, ((char*)field.props),
        field.numValidProps * sizeof(ScalarProperty_t), node_zero);

      /* Write current values to the file. */
      for(i = 0; i < field.numValidProps; i++) {
        WRITE_VECTOR_TO_FILE(pFile, "%+1.012e", field.props[i].location);
        fprintf(pFile, "  ");
        WRITE_VECTOR_TO_FILE(pFile, "%+1.012e", field.props[i].areaNormal);
        fprintf(pFile, "  %+1.012e\n", field.props[i].value);
      }

      free(field.props);
    }
  }
#endif /* RP_HOST */

#if !PARALLEL
  /* serial processing only */
  /* Write current values to the file. */
  for(i = 0; i < field.numValidProps; i++) {
    WRITE_VECTOR_TO_FILE(pFile, "%+1.012e", field.props[i].location);
    fprintf(pFile, "  ");
    WRITE_VECTOR_TO_FILE(pFile, "%+1.012e", field.props[i].areaNormal);
    fprintf(pFile, "  %+1.012e\n", field.props[i].value);
  }

  free(field.props);
#endif /* !PARALLEL */
}

/*******************************************************************************
 * Writes a vector field to a file. The field will be retrieved by the given
 * function pointer. The property vectors, location vectors and area normal
 * vectors will be written to the file.
 *
 * @param pDomain The domain from which to read the property vector field.
 * @param zoneId The id of the zone in the domain from which the property vector
 *   field should be read.
 * @param pFile The file handle to write the data to.
 * @param propName The name of the vector field property, will be used for
 *   the column name of the property vectors in the file.
 * @param pfGetVectorPropField The function pointer to retrieve the property
 *   vector field.
 */
void writeVectorPropFieldToFile(Domain* pDomain, ZoneId_t zoneId, FILE* pFile,
  const char* propName, GetVectorPropField_t pfGetVectorPropField) {
  VectorPropField_t field;

#if !RP_HOST
  Thread* pThread = NULL;
#endif /* !RP_HOST */

#if PARALLEL
  int compNode = 0;
#endif /* PARALLEL */

#if !RP_NODE
  int i = 0;
#endif /* !RP_NODE */

  /* Initialize variables. */
  field.maxProps = 0;
  field.numValidProps = 0;
  field.props = NULL;

#if !RP_NODE
  /* Write file header. */
  if (pFile != NULL) {
    if (ND_ND == 2) {
      fprintf(pFile,
        "\"location\"                               "
      /* +0.000000000000e-00 +0.000000000000e-00  */
        "\"area normal\"                            "
      /* +0.000000000000e-00 +0.000000000000e-00  */
        "\"%s\"\n"
      /* +0.000000000000e-00 +0.000000000000e-00 */
        "\"x-component\"       \"y-component\"        "
      /* +0.000000000000e-00 +0.000000000000e-00  */
        "\"x-component\"       \"y-component\"        "
      /* +0.000000000000e-00 +0.000000000000e-00  */
        "\"x-component\"       \"y-component\"\n", propName);
      /* +0.000000000000e-00 +0.000000000000e-00 */
    } else if (ND_ND == 3) {
      fprintf(pFile,
        "\"location\"                                                   "
      /* +0.000000000000e-00 +0.000000000000e-00 +0.000000000000e-00  */
        "\"area normal\"                                                "
      /* +0.000000000000e-00 +0.000000000000e-00 +0.000000000000e-00  */
        "\"%s\"\n"
      /* +0.000000000000e-00 +0.000000000000e-00 +0.000000000000e-00 */
        "\"x-component\"       \"y-component\"       \"z-component\"        "
      /* +0.000000000000e-00 +0.000000000000e-00 +0.000000000000e-00  */
        "\"x-component\"       \"y-component\"       \"z-component\"        "
      /* +0.000000000000e-00 +0.000000000000e-00 +0.000000000000e-00  */
        "\"x-component\"       \"y-component\"       \"z-component\"\n",
      /* +0.000000000000e-00 +0.000000000000e-00 +0.000000000000e-00 */
        propName);
    }
  }
#endif /* !RP_NODE */

#if !RP_HOST
  /* Every compute node retrieves the vector field of the property. */
  pThread = Lookup_Thread(pDomain, zoneId);
  field = pfGetVectorPropField(pThread);

# if RP_NODE
  /* compute nodes only */
  /* Every compute node, except compute node zero sends their field to the
   * compute node zero.
   */
  if (!I_AM_NODE_ZERO_P) {
    PRF_CSEND_INT(node_zero, &field.numValidProps, 1, myid);

    /* Skip if there are no properties to send. */
    if (field.numValidProps > 0) {
      PRF_CSEND_CHAR(node_zero, ((char*)field.props),
        field.numValidProps * sizeof(VectorProperty_t), myid);
    }

    free(field.props);
  }

  /* Compute node zero sends it`s field to the host, than retrieves the fields
   * of the other compute nodes and sends them also to the host.
   */
  if (I_AM_NODE_ZERO_P) {
    /* Send node zero data to host. */
    PRF_CSEND_INT(node_host, &field.numValidProps, 1, myid);

    /* Skip if there are no properties to send. */
    if (field.numValidProps > 0) {
      PRF_CSEND_CHAR(node_host, ((char*)field.props),
        field.numValidProps * sizeof(VectorProperty_t), myid);
    }

    free(field.props);
    field.props = NULL;

    /* Receive data from the compute nodes and send them to the host. */
    compute_node_loop_not_zero(compNode) {
      /* Get number of valid properties to receive from compute node. */
      PRF_CRECV_INT(compNode, &field.numValidProps, 1, compNode);
      field.maxProps = field.numValidProps;

      /* Skip if there are no properties to receive. */
      if (field.numValidProps > 0) {
        /* Allocate memory for the properties to receive. */
        field.props = (VectorProperty_t*)malloc(
          field.numValidProps * sizeof(VectorProperty_t));

        /* Check if successfully allocated memory. */
        if (field.props == NULL) {
          Message("Failed to allocate %d memory for a vector field.\n",
            field.numValidProps * sizeof(VectorProperty_t));
          return;
        }

        /* Retrieve data from the compute node. */
        PRF_CRECV_CHAR(compNode, ((char*)field.props),
          field.numValidProps * sizeof(VectorProperty_t), compNode);
      }

      /* Send received compute node data from compute node zero data to the
      * host.
      */
      PRF_CSEND_INT(node_host, &field.numValidProps, 1, myid);

      /* Skip if there are no properties to receive. */
      if (field.props != NULL) {
        if (field.numValidProps > 0) {
          PRF_CSEND_CHAR(node_host, ((char*)field.props),
            field.numValidProps * sizeof(VectorProperty_t), myid);
        }

        free(field.props);
        field.props = NULL;
      }
    }
  }
# endif /* RP_NODE */
#endif /* !RP_HOST */

#if RP_HOST
  compute_node_loop(compNode) {
    /* Get number of valid properties to receive from compute node. */
    PRF_CRECV_INT(node_zero, &field.numValidProps, 1, node_zero);
    field.maxProps = field.numValidProps;

    /* Skip if there are no properties to receive. */
    if (field.numValidProps > 0) {
      /* Allocate memory for the properties to receive. */
      field.props = (VectorProperty_t*)malloc(
        field.numValidProps * sizeof(VectorProperty_t));

      /* Check if successfully allocated memory. */
      if (field.props == NULL) {
        Message("Failed to allocate %d memory for a vector field.\n",
          field.numValidProps * sizeof(VectorProperty_t));
        return;
      }

      /* Retrieve data from the compute node. */
      PRF_CRECV_CHAR(node_zero, ((char*)field.props),
        field.numValidProps * sizeof(VectorProperty_t), node_zero);

      /* Write current values to the file. */
      for(i = 0; i < field.numValidProps; i++) {
        WRITE_VECTOR_TO_FILE(pFile, "%+1.012e", field.props[i].location);
        fprintf(pFile, "  ");
        WRITE_VECTOR_TO_FILE(pFile, "%+1.012e", field.props[i].areaNormal);
        fprintf(pFile, "  ");
        WRITE_VECTOR_TO_FILE(pFile, "%+1.012e", field.props[i].value);
        fprintf(pFile, "\n");
      }

      free(field.props);
    }
  }
#endif /* RP_HOST */

#if !PARALLEL
  /* serial processing only */
  /* Write current values to the file. */
  for(i = 0; i < field.numValidProps; i++) {
    WRITE_VECTOR_TO_FILE(pFile, "%+1.012e", field.props[i].location);
    fprintf(pFile, "  ");
    WRITE_VECTOR_TO_FILE(pFile, "%+1.012e", field.props[i].areaNormal);
    fprintf(pFile, "  ");
    WRITE_VECTOR_TO_FILE(pFile, "%+1.012e", field.props[i].value);
    fprintf(pFile, "\n");
  }

  free(field.props);
#endif /* !PARALLEL */
}
